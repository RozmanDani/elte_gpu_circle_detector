__kernel void center_radius(

  __global const int *randX1,
  __global const int *randY1,

  __global const int *randX2,
  __global const int *randY2,

  __global const int *randX3,
  __global const int *randY3,

  __global int *cXOutput,
  __global int *cYOutput,
  __global int *rOutput)
{ 

    // Get the index of the current element to be processed
    int i = get_global_id(0);


    //get random generated points
    float x1 = randX1[i];  
    float y1 = randY1[i];

    float x2 = randX2[i];
    float y2 = randY2[i];

    float x3 = randX3[i];
    float y3 = randY3[i];

    //find the circle from 3 random generated points
    //DISTANCE BETWEEN POINTS
    int x12 = x1 - x2;
    int x13 = x1 - x3;
  
    int y12 = y1 - y2;
    int y13 = y1 - y3;
  
    int y31 = y3 - y1;
    int y21 = y2 - y1;
  
    int x31 = x3 - x1;
    int x21 = x2 - x1;

    int sx13 = (int)((x1*x1) - (x3*x3));
    int sy13 = (int)((y1*y1) - (y3*y3));

    int sx21 = (int)((x2*x2) - (x1*x1));
    int sy21 = (int)((y2*y2) - (y1*y1));
    
    int f = ((sx13) * (x12) 
            + (sy13) * (x12) 
            + (sx21) * (x13) 
            + (sy21) * (x13)) 
            / (2 * ((y31) * (x12) - (y21) * (x13))); 
    int g = ((sx13) * (y12) 
            + (sy13) * (y12) 
            + (sx21) * (y13) 
            + (sy21) * (y13)) 
            / (2 * ((x31) * (y12) - (x21) * (y13))); 
  
    // Use form: x^2 + y^2 + 2*g*x + 2*f*y + c = 0 
    // where centre is (h = -g, k = -f) and radius r 
    // as r^2 = h^2 + k^2 - c 
    int c = -(int)(x1*x1) - (int)(y1*y1) - 2 * g * x1 - 2 * f * y1; 

    //CENTER CORDINATE X IS 'h'
    int h = -g; 

    //CENTER CORDINATE Y IS 'k'
    int k = -f; 

    //RADIUS
    int sqr_of_r = h * h + k * k - c; 
    float r = sqrt((float)(sqr_of_r)); 

   //OUTPUT
   cXOutput[i] = h;
   cYOutput[i] = k;
   rOutput[i] = r;
}


