#define __NO_STD_VECTOR
#define __CL_ENABLE_EXCEPTIONS
#define min(a,b)            (((a) < (b)) ? (a) : (b))

#include <CL/cl.hpp>
#include <utility>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h> 

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <oclutils.hpp>

using namespace cl;


size_t split(const std::string& txt, std::vector<std::string>& strs, char ch);

bool isOnCircle(int circle_x, int circle_y, int rad, int x, int y);

int main() {

	std::string filename = "points4.txt";
	std::string imageName = "image4.png";
	

	std::string line;

	std::ifstream myfile(filename);
	std::ifstream forLength(filename);

	const int LIST_SIZE = std::count(std::istreambuf_iterator<char>(forLength), //Get the number of lines
		std::istreambuf_iterator<char>(), '\n');
	forLength.close();

	int* x = new int[LIST_SIZE];
	int* y = new int[LIST_SIZE];

	int count = 0;

	if (myfile.is_open())
	{
		while (std::getline(myfile, line))
		{
			std::vector<std::string> v;
			split(line, v, ' ');


			x[count] = stoi(v[0]);
			y[count] = stoi(v[1]);
			count++;
		}
		myfile.close();

	}
	else std::cout << "Unable to open file";

	//Generated random points
	const int RANDOM_POINTS_COUNT = LIST_SIZE * 5;
	srand(time(0)); //every time we run the program different random number generated

	int* randX1 = new int[RANDOM_POINTS_COUNT];
	int* randY1 = new int[RANDOM_POINTS_COUNT];

	int* randX2 = new int[RANDOM_POINTS_COUNT];
	int* randY2 = new int[RANDOM_POINTS_COUNT];

	int* randX3 = new int[RANDOM_POINTS_COUNT];
	int* randY3 = new int[RANDOM_POINTS_COUNT];

	for (int i = 0; i < RANDOM_POINTS_COUNT; i++) {
		randX1[i] = x[rand() % LIST_SIZE];
		randY1[i] = y[rand() % LIST_SIZE];

		randX2[i] = x[rand() % LIST_SIZE];
		randY2[i] = y[rand() % LIST_SIZE];

		randX3[i] = x[rand() % LIST_SIZE];
		randY3[i] = y[rand() % LIST_SIZE];
	}

	try {
		// Get available platforms
		vector<Platform> platforms;
		Platform::get(&platforms);

		vector<Device> devices; 
		Context context;

		for (auto p : platforms) {

			try {
				std::cout << p.getInfo<CL_PLATFORM_NAME>() << std::endl;
				std::cout << p.getInfo<CL_PLATFORM_VERSION>() << std::endl;

				// Select the default platform and create a context using this platform and the GPU
				cl_context_properties cps[3] = {
					CL_CONTEXT_PLATFORM,
					(cl_context_properties)(p)(),
					0
				};

				context = Context(CL_DEVICE_TYPE_GPU, cps);

				// Get a list of devices on this platform
				devices = context.getInfo<CL_CONTEXT_DEVICES>();

			}
			catch (Error error) {
				oclPrintError(error);
				continue;
			}

			if (devices.size() > 0)
				break;
		}

		if (devices.size() == 0) {
			throw Error(CL_INVALID_CONTEXT, "Failed to create a valid context!");
		}

		// Create a command queue and use the first device
		CommandQueue queue = CommandQueue(context, devices[0]);

		// Read source file
		std::ifstream sourceFile("center_radius_kernel.cl");
		std::string sourceCode(
			std::istreambuf_iterator<char>(sourceFile),
			(std::istreambuf_iterator<char>()));
		Program::Sources source(1, std::make_pair(sourceCode.c_str(), sourceCode.length() + 1));

		// Make program of the source code in the context
		Program program = Program(context, source);

		// Build program for these specific devices
		program.build(devices);

		// Make kernel
		Kernel kernel(program, "center_radius");
		
		//Create memory buffers, to store possible cirles
		Buffer cXBuffer = Buffer(context, CL_MEM_READ_WRITE, sizeof(int) * RANDOM_POINTS_COUNT); //READ AND WRITE BUFFER
		Buffer cYBuffer = Buffer(context, CL_MEM_READ_WRITE, sizeof(int) * RANDOM_POINTS_COUNT); //READ AND WRITE BUFFER
		Buffer rBuffer = Buffer(context, CL_MEM_READ_WRITE, sizeof(int) * RANDOM_POINTS_COUNT); //READ AND WRITE BUFFER

		//Create memory buffers, to store random generated points
		Buffer randX1Buffer = Buffer(context, CL_MEM_READ_ONLY, sizeof(int) * RANDOM_POINTS_COUNT);
		Buffer randY1Buffer = Buffer(context, CL_MEM_READ_ONLY, sizeof(int) * RANDOM_POINTS_COUNT);

		Buffer randX2Buffer = Buffer(context, CL_MEM_READ_ONLY, sizeof(int) * RANDOM_POINTS_COUNT);
		Buffer randY2Buffer = Buffer(context, CL_MEM_READ_ONLY, sizeof(int) * RANDOM_POINTS_COUNT);

		Buffer randX3Buffer = Buffer(context, CL_MEM_READ_ONLY, sizeof(int) * RANDOM_POINTS_COUNT);
		Buffer randY3Buffer = Buffer(context, CL_MEM_READ_ONLY, sizeof(int) * RANDOM_POINTS_COUNT);

		/// Write data on input buffers!	
		queue.enqueueWriteBuffer(randX1Buffer, CL_TRUE, 0, sizeof(int) * RANDOM_POINTS_COUNT, randX1);
		queue.enqueueWriteBuffer(randY1Buffer, CL_TRUE, 0, sizeof(int) * RANDOM_POINTS_COUNT, randY1);

		queue.enqueueWriteBuffer(randX2Buffer, CL_TRUE, 0, sizeof(int)* RANDOM_POINTS_COUNT, randX2);
		queue.enqueueWriteBuffer(randY2Buffer, CL_TRUE, 0, sizeof(int)* RANDOM_POINTS_COUNT, randY2);

		queue.enqueueWriteBuffer(randX3Buffer, CL_TRUE, 0, sizeof(int)* RANDOM_POINTS_COUNT, randX3);
		queue.enqueueWriteBuffer(randY3Buffer, CL_TRUE, 0, sizeof(int)* RANDOM_POINTS_COUNT, randY3);
		
		// Set arguments to kernel		
		kernel.setArg(0, randX1Buffer);
		kernel.setArg(1, randY1Buffer);

		kernel.setArg(2, randX2Buffer);
		kernel.setArg(3, randY2Buffer);

		kernel.setArg(4, randX3Buffer);
		kernel.setArg(5, randY3Buffer);

		kernel.setArg(6, cXBuffer);
		kernel.setArg(7, cYBuffer);
		kernel.setArg(8, rBuffer);
		
		// Run the kernel on specific ND range
		NDRange _global_(RANDOM_POINTS_COUNT);
		queue.enqueueNDRangeKernel(kernel, cl::NullRange, _global_, cl::NullRange);

		// Read buffer C (the result) into a local piece of memory
		int* cX = new int[RANDOM_POINTS_COUNT];
		int* cY = new int[RANDOM_POINTS_COUNT];
		int* r = new int[RANDOM_POINTS_COUNT];
		queue.enqueueReadBuffer(cXBuffer, CL_TRUE, 0, sizeof(int) * RANDOM_POINTS_COUNT, cX);
		queue.enqueueReadBuffer(cYBuffer, CL_TRUE, 0, sizeof(int) * RANDOM_POINTS_COUNT, cY);
		queue.enqueueReadBuffer(rBuffer, CL_TRUE, 0, sizeof(int) * RANDOM_POINTS_COUNT, r);

		// Check the results!
		for (int i = 0; i < RANDOM_POINTS_COUNT; i++)
			std::cout << "(" <<randX1[i] << "," << randY1[i] << ") " 
					  << "(" << randX2[i] << "," << randY2[i] << ") "
			          << "(" << randX3[i] << "," << randY3[i] << ") -> x: "
			          << cX[i] << " y: " 
			          << cY[i] << " radius: " 
			          << r[i] << std::endl;


		//Array that stores how many points are on that circle
		int* pointCount = new int[RANDOM_POINTS_COUNT];

		//First all circle has 0 points
		for (int i = 0; i < RANDOM_POINTS_COUNT; i++) {
			pointCount[i] = 0;
		}
		
		//If the point is on the circle, than add it
		for (int i = 0; i < RANDOM_POINTS_COUNT; i++) {
			for (int j = 0; j < LIST_SIZE; j++) {
				if (isOnCircle(cX[i], cY[i], r[i], x[j], y[j])) {
					pointCount[i]++;
				}
			}
		}
		
		//Max search, find the most stable circle, which 
		int maxxIndex = 0;
		int maxx = 0;
		for (int i = 0; i < RANDOM_POINTS_COUNT; i++) {
			if (pointCount[i] > maxx) {
				maxx = pointCount[i];
				maxxIndex = i;
			}
		}

		std::cout << "Most supported points:" << maxx << " cX:" << cX[maxxIndex] << " cY:"<< cY[maxxIndex] << " r: "<<r[maxxIndex] << std::endl;

		
		// Draw and save circle
		cv::Mat image = cv::imread(imageName, 0);

		cv::cvtColor(image, image, CV_GRAY2RGB);

		circle(image, cv::Point(cX[maxxIndex], cY[maxxIndex]), r[maxxIndex], cv::Scalar(0, 255, 0),5);
		
		cv::Mat resized;
		cv::resize(image, resized, cv::Size(), 0.3, 0.3);

		cv::imshow("circles", resized);
		cv::imwrite("output.jpg", resized);
		cv::waitKey();
		
		

	}
	catch (Error error) {
		oclPrintError(error);
	}

	std::cin.get();

	return 0;
}


size_t split(const std::string& txt, std::vector<std::string>& strs, char ch)
{
	size_t pos = txt.find(ch);
	size_t initialPos = 0;
	strs.clear();

	// Decompose statement
	while (pos != std::string::npos) {
		strs.push_back(txt.substr(initialPos, pos - initialPos));
		initialPos = pos + 1;

		pos = txt.find(ch, initialPos);
	}

	// Add the last one
	strs.push_back(txt.substr(initialPos, std::min(pos, txt.size()) - initialPos + 1));

	return strs.size();
}


bool isOnCircle(int circle_x, int circle_y,
	int rad, int x, int y)
{
	float distance = sqrt(pow((x - circle_x), 2) + pow((y - circle_y), 2));

	if ((int)distance == rad)
	{
		return true;
	}
	else
	{
		return false;
	}
}