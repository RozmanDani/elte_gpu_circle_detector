Circle Detector program

The task is to detect circles from the contour points of a sphere in an image. 
The implemented method has to be robust: it can deal with self-occlusion.
